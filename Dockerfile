FROM node:19-alpine3.16
LABEL cowsay.version=v2
RUN apk update && apk add bash
ENV PORT=8080
WORKDIR /var/node
COPY src/ /var/node
RUN npm install
COPY entry-point.sh /var/node
ENTRYPOINT ./entry-point.sh $PORT

